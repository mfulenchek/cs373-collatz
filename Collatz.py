#!/usr/bin/env python3

''' ------------------
# collatz/Collatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ------------------
'''

from typing import IO, List

GLOBAL_CYCLE = {}


def collatz_read(input_string: str) -> List[int]:
    '''
    read two ints
    input_string a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    '''
    an_input = input_string.split()
    return [int(an_input[0]), int(an_input[1])]


def collatz_eval(begin_range: int, end_range: int) -> int:
    '''
        begin_range the beginning of the range, inclusive
        end_range the end       of the range, inclusive
        check beginning bigger than end, swap it
        build global dictionary for cache
        return the max cycle length of the range [begin_range, end_range]
    '''
    if begin_range > end_range:
        temp_range = begin_range
        begin_range = end_range
        end_range = temp_range

    global GLOBAL_CYCLE
    middle_range = (end_range // 2) + 1

    if begin_range < middle_range:
        begin_range = middle_range

    max_value = 1
    for num in range(begin_range, end_range + 1):
        initial_val = num
        cycle_len = 1
        while num > 1:
            if num in GLOBAL_CYCLE:
                cycle_len += (GLOBAL_CYCLE.get(num) - 1)
                num = 1
            elif (num % 2) == 0:
                num = (num >> 1)
                cycle_len += 1
            else:
                num = (num + (num >> 1) + 1)
                cycle_len += 2
        if cycle_len > max_value:
            max_value = cycle_len
        GLOBAL_CYCLE[initial_val] = cycle_len
    return max_value


def collatz_print(write_value: IO[str], begin_rang: int, end_rang: int, value_output: int) -> None:
    '''
    # -------------
    # collatz_print
    # -------------
    print three ints
    write_values a writer
    begin_range the beginning of the range, inclusive
    end_range the end       of the range, inclusive
    value_outputs the max cycle length
    '''
    write_value.write(str(begin_rang) + " " + str(
        end_rang) + " " + str(value_output) + "\n")


def collatz_solve(read_values: IO[str], write_values: IO[str]) -> None:
    '''
    r a reader
    w a writer
    # -------------
    # collatz_solve
    # -------------
    '''
    for input_str in read_values:
        begin_range, end_range = collatz_read(input_str)
        value_outputs = collatz_eval(begin_range, end_range)
        collatz_print(write_values, begin_range, end_range, value_outputs)

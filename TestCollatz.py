#!/usr/bin/env python3
'''
# ----------------------
# collatz/TestCollatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ----------------------

# -------
# imports
# -------
'''

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve


class TestCollatz(TestCase):

    ''' -----------
        read
        -----------
    '''

    def test_read(self):
        ''' -----------
            read values
            -----------
        '''
        input_string = "1 10\n"
        begin_range, end_range = collatz_read(input_string)
        self.assertEqual(begin_range, 1)
        self.assertEqual(end_range, 10)

    def test_eval_1(self):
        ''' -----------
            test range 1-10
            -----------
        '''
        max_value = collatz_eval(1, 10)
        self.assertEqual(max_value, 20)

    def test_eval_2(self):
        ''' -----------
            test range 100-200
            -----------
        '''
        max_value = collatz_eval(100, 200)
        self.assertEqual(max_value, 125)

    def test_eval_3(self):
        ''' -----------
            test range 201-210
            -----------
        '''
        max_value = collatz_eval(201, 210)
        self.assertEqual(max_value, 89)

    def test_eval_4(self):
        ''' -----------
            test range 900-1000
            -----------
        '''
        max_value = collatz_eval(900, 1000)
        self.assertEqual(max_value, 174)

    def test_eval_5(self):
        ''' -----------
            test range largest range
            -----------
        '''
        max_value = collatz_eval(106, 210)
        self.assertEqual(max_value, 125)

    def test_eval_6(self):
        ''' -----------
            test range beginning larger than end
            -----------
        '''
        max_value = collatz_eval(10, 1)
        self.assertEqual(max_value, 20)

    def test_eval_7(self):
        ''' -----------
            test small range
            -----------
        '''
        max_value = collatz_eval(6, 10)
        self.assertEqual(max_value, 20)

    def test_eval_8(self):
        ''' -----------
            test no range
            -----------
        '''
        max_value = collatz_eval(26, 26)
        self.assertEqual(max_value, 11)

    def test_eval_9(self):
        ''' -----------
            test only 1
            -----------
        '''
        max_value = collatz_eval(1, 1)
        self.assertEqual(max_value, 1)

    def test_eval_10(self):
        ''' -----------
            test 210-600
            -----------
        '''
        max_value = collatz_eval(210, 600)
        self.assertEqual(max_value, 144)

    def test_eval_11(self):
        ''' -----------
            test begin larger than end bigger range
            -----------
        '''
        max_value = collatz_eval(210, 80)
        self.assertEqual(max_value, 125)

    def test_print(self):
        ''' -----------
            print
            -----------
        '''
        write_back = StringIO()
        collatz_print(write_back, 1, 10, 20)
        self.assertEqual(write_back.getvalue(), "1 10 20\n")

    def test_solve(self):
        ''' -----------
            solve
            -----------
        '''
        read_in = StringIO(
            "1 10\n100 200\n201 210\n900 1000\n106 210\n10 1\n6 10\n26 26\n1 1\n210 600\n210 80\n")
        write_back = StringIO()
        collatz_solve(read_in, write_back)
        self.assertEqual(
            write_back.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n106 210 125\n10 1 20\n6 10 20\n26 26 11\n1 1 1\n210 600 144\n210 80 125\n")

if __name__ == "__main__":  # pragma: no cover
    main()
